#' Add a variable that names the type of matter
#'
#' String recognition of elemental matter, e.g. CHO, CHON, etc
#' @param DataAtoms A dataframe from Mzmine that contains atomic number variables
#' @export


MatterType<- function (DataAtoms) {
  DataAtoms <- DataAtoms %>%
    mutate(Type.Matter = if_else(
      H > 0 & C > 0 & O > 0 & N == 0 & P == 0 & S == 0 & Cl == 0 & Br == 0 & Fl == 0 & I == 0,
      "CHO", # first cond
      if_else(
        H > 0 & C > 0 & O > 0 & N > 0 & P == 0 & S == 0 & Cl == 0 & Br == 0 & Fl == 0 & I == 0,
        "CHON", # second cond
        if_else(
          H > 0 & C > 0 & O > 0 & N > 0 & P > 0 & S == 0 & Cl == 0 & Br == 0 & Fl == 0 & I == 0,
          "CHONP",
          if_else(
            H > 0 & C > 0 & O > 0 & N > 0 & S > 0 & P == 0 & Cl == 0 & Br == 0 & Fl == 0 & I == 0,
            "CHONS",
            if_else(
              H > 0 & C > 0 & O > 0 & N > 0 & S > 0 & P > 0 & Cl == 0 & Br == 0 & Fl == 0 & I == 0,
              "CHONPS",
              if_else(
                Cl > 0 | Br > 0 | Fl > 0 | I > 0, # last condition and container for not defined
                "with Halogens", "other"
              )
            )
          )
        )
      )
    ))
}
