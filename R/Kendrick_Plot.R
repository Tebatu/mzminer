#' Kendrick (Mass Defect) plot with all masses, but series are highlighted.
#'
#' Plot of nominal Kendrick mass vs. Kendrick mass defect with homologous series highlited by red lines.
#' The function is based on variables from "ComputeKendrick and "ExtractHomolog" functions.
#' @param DataSeries A dataframe with masses grouped by homologous series, which will be shown as red lines.
#' @param DataPoints A dataframe with all the features in the dataset, which will be shown as points.
#' @param NumberHomologues Pick a minimum, integer number of homologues to be displayed in plot title.          
#' @param PointSize Size of the points displayed in the plot.  
#' @export        
#' @examples LcmsData <- data.frame(x = runif(20, min=0, max=100))
#' LcmsWithKendrick <- ComputeKendrick(LcmsData, LcmsData$x, "CH2", 1)
#' Homologues <- ExtractHomolog(LcmsWithKendrick, 3)
#' KendrickPlot(Homologues, LcmsWithKendrick, 3, 3.4)


KendrickPlot <- function (DataSeries, DataPoints, NumberHomologues, PointSize) {
  ggplot (data = DataSeries, aes(x = KendrickMassInteger, y = RoundDeffect, group = DeffectFactor)) +
    geom_line(colour = "red") +
    geom_point(size = PointSize, shape = 21, fill = "red") +
    geom_point(data = DataPoints, aes (KendrickMassInteger, RoundDeffect)) +
    xlab("Kendrick Nominal Mass [Da]") + ylab("Kendrick Mass Defect [Da]") +
    theme_bw() +
    scale_x_continuous(breaks = pretty(DataPoints$KendrickMassInteger, n = 10)) +
    scale_y_continuous(breaks = pretty(as.numeric(DataPoints$RoundDeffect), n = 12))+
    ggtitle(paste("Kendrick plot: series of homologues >", NumberHomologues, "are shown in red"))  
}

