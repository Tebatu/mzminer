#' Add mean, standard deviation and  CV for a property for a sample.
#'
#' Mean, Standard deviation Coefficient of Variation and the number of replicates (not NAs, so caution with 0) used are computed for a sample.
#' @param MzmineData Dataframe with the imported data of Mzmine for a sample set, requires intensities of replicates. 
#' @param SampleId The shortest character string identical for all names of the sample,
#' at the beginning of the name or within it.
#' @param PropertyId The shortest character string identical for all names of the property,
#' @param id.contained FALSE by default. If TRUE the name specified in SampleId is 
#' picked by the smallest similar contained string.
#' @export       
#' @examples RoWithStats <- SampleStats(RoBalancedMzmine_PI, "YV20")
#' RoWithStats <- SampleStats(RoWithStats, "YV23")

SampleStats <- function (MzmineData, SampleId, PropertyId, id.contained) {
  
  Replicates <- MzmineData %>% select(contains(PropertyId)) # select which property to average
  
  if(missing(id.contained)) { # select replicates of a sample by name 
    Replicates <- Replicates %>% select(starts_with(SampleId))
  } else if(id.contained == FALSE) {
    Replicates <- Replicates %>% select(starts_with(SampleId)) 
  } else {
    Replicates <- Replicates %>% select(contains(SampleId))
  }
  
  print(paste(names(Replicates))) # Look up in console which replicates are selected

  AverageSample <- rowMeans(Replicates, na.rm = TRUE) # Calculate mean values of the set
  
  MzmineData <- MzmineData %>% mutate(PropertyNonorm  = AverageSample) # Add the mean variable to the dataframe

  MzmineData <- MzmineData %>%  # Add standard deviations of Propertys and number of used replicates (not NA in the raw data) to the dataframe
    mutate(PropertySd  = apply(Replicates, 1, sd, na.rm=TRUE),
           PropertyCount = apply(Replicates, 1, function(x) sum(!is.na(x))))
  
  # Compute coefficient of variation (percentage)
  Cv <- function (Mean, Sd){   # Standard deviation divided by mean and multiplied by 100. 
    (Sd/Mean)*100 # Sd is computed with denominator (X-1)
  }
  
  #Add CVs variable to the dataframe
  MzmineData <- MzmineData %>% 
    mutate(PropertyCv = Cv(MzmineData$PropertyNonorm, MzmineData$PropertySd))
  
  # Rename the output consistently
  names(MzmineData) <- gsub("PropertyNonorm", paste(SampleId, "Mean", PropertyId, sep = "."), names(MzmineData), fixed = TRUE)
  names(MzmineData) <- gsub("PropertySd", paste(SampleId, "Sd", PropertyId, sep = "."), names(MzmineData), fixed = TRUE)
  names(MzmineData) <- gsub("PropertyCv", paste(SampleId, "Cv", PropertyId, sep = "."), names(MzmineData), fixed = TRUE)
  names(MzmineData) <- gsub("PropertyCount", paste(SampleId, "Repl.Count", PropertyId, sep = "."), names(MzmineData), fixed = TRUE)
  
  return(MzmineData)
}