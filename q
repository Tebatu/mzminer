[33mcommit f0a02678f1af582e10881e6e1003ed664615022d[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 12 12:57:26 2016 +0000

    feat: Add a function to distinguish types of matter, e.g CHO, CHON.

[33mcommit 1806a3a11ae274334ce059573a827c1af8b13c82[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 12 10:09:11 2016 +0000

    Fix: display the correct amount of features with formulas ONLY.

[33mcommit 27d2e024b7162cee383db2d9c969ebdc02e2ff44[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Tue Feb 9 10:05:41 2016 +0000

    feat: add a quick fix to remove m/z duplicates from the dataset
    
    This is needed because the duplicated m/z causes problems with ids in later functions.

[33mcommit 9aee3cd1916803f70737892ef871f6110b7f7632[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 5 12:31:04 2016 +0000

    test: confirm that the functions of MzMineR work scientifically correctly on expample of RO

[33mcommit dcdd15dbfd943aeffc8c2dd456921820a8684022[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 5 12:02:36 2016 +0000

    feat: add calculation of total features and elemental percentages.

[33mcommit cb5abafcc65d65bc76e5a9c4275486d636258c82[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 5 11:02:31 2016 +0000

    feat: Make a fn to compute number of appeared/disappeared features

[33mcommit 47fbc577cf6a635b08528b0f7e1c6d697c75e108[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 5 11:00:56 2016 +0000

    feat: Repurpose fn for unification of PI and NI, rename the fn used before

[33mcommit 53d3e3cb2c1796d63aded8e6644ac3cf67764c0b[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Fri Feb 5 10:58:59 2016 +0000

    fix: change variable to reference mass for mass defect

[33mcommit d3d460c0fc522d3232e61b8a6f938733ad7fbe6d[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 22:19:00 2016 +0000

    fix: Change the ratio from B/A to A/B to correct the dfs

[33mcommit 0f62dd458fd3f2ea39f58892bb6a1b13f84adf2b[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 21:46:40 2016 +0000

    Fix
    
    Resolve the problem with the ratio column name in AddRatio and with the bind_rows() fn of AddAtoms
    
    The long name was causing problems with bind_rows

[33mcommit ee13ba6b25cd9c1c197314d8c36be8fbcc96d8c1[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 17:30:29 2016 +0000

    fix
    
    Deleted bindrows command that causes r to crash.

[33mcommit 0a0ae3fc2b1e7ba5726e9380e2b76b2353fb3b44[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 16:30:31 2016 +0000

    Fix
    
    Change the order of merged dataframes so that features with ratio 0 (not appearing in that sample) are not shown

[33mcommit 230338d94b2de49934b12457cd04adf0c1a56aec[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 15:50:28 2016 +0000

    feat
    
    Compute the ratio of intensities between two samples and tell how intensity changed between two samples.

[33mcommit 1822b957501fa8f9d9946ff6a75cca7f969f8e5b[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 13:09:03 2016 +0000

    feat
    
    Remove features with intensity = 0 and filter the big data table to contain only features with Int > 0

[33mcommit 0637b81a980fd433c98937b81349223052c93442[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 11:19:28 2016 +0000

    fix
    
    Change fn for id recognition FilterCv and correct the subtraction recognition in SubtractBlank
    
    A set of errors with matches() was fixed. The character string for blank id was changed and the number of Intensity column was changed.

[33mcommit 07598f388d7e09fe78f34397fda7f8f898e604c5[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Thu Feb 4 10:50:12 2016 +0000

    Fix
    
    Separate id recognition fn in FilterCv

[33mcommit 23572a63b8e1968a31a8543b9f12b8456ac01dd0[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 16:00:01 2016 +0000

    test/docs
    
    Confirm FilterCv works well. Change description of function in the documentation.

[33mcommit c90565054c3bde7d95a572a4750269a2ef2841d1[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 15:50:06 2016 +0000

    test
    
    Confirm SampleStats works correctly

[33mcommit 686e2f716e2ca725b025d8feada1725233407b68[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 15:46:55 2016 +0000

    fix
    
    Change the old ids of samples to new ones

[33mcommit dbe0b5c71de5c9b99bc88ca27db9c23bd7d649ab[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 15:23:46 2016 +0000

    feat
    
    Subtract the blank from the sample and rename the old RO function

[33mcommit 83922b5adb724eb26b1d2a5f9a71c548a762c7b0[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 15:03:28 2016 +0000

    docs/fix
    
    Improve documentation. Add the identifier for blank sample

[33mcommit bf47a735c26991257bf6357cf2a7e4430d670df4[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 14:56:22 2016 +0000

    Add Filter by CV function, also rename the old RO function to Old_x

[33mcommit 429ffc60914886f0a903def27b86fc2dc93f4854[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 14:08:30 2016 +0000

    Compute mean, sd, cv for a sample with function SampleStat

[33mcommit ed5c0aefd91e79fb1177c89d6caa3af9f37ac6a8[m
Author: Yaroslav Verkh <yaroslav.verkh@verkh.de>
Date:   Wed Feb 3 13:50:25 2016 +0000

    Prepare working RO analysis
