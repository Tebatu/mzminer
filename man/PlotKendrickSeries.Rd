% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Plot_Kendrick_Series.R
\name{PlotKendrickSeries}
\alias{PlotKendrickSeries}
\title{Kendrick (Mass Defect) plot of homologous series only.}
\usage{
PlotKendrickSeries(DataSeries, NumberHomologues, PointSize)
}
\arguments{
\item{DataSeries}{A dataframe with masses grouped by homologous series, which will be shown as red lines.}

\item{NumberHomologues}{Pick a minimum, integer number of homologues to be displayed in plot title.}

\item{PointSize}{Size of the points displayed in the plot.}
}
\description{
Plot of nominal Kendrick mass vs. Kendrick mass defect only for homologous series for a selected normalizing moiety.
The function is based on variables from "ComputeKendrick and "ExtractHomolog" functions.
}
\examples{
LcmsData <- data.frame(x = runif(20, min=0, max=100))
LcmsWithKendrick <- ComputeKendrick(LcmsData, LcmsData$x, "CH2", 1)
Homologues <- ExtractHomolog(LcmsWithKendrick, 3)
PlotKendrickSeries(Homologues, 3, 3.4)
}

